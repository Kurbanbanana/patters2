﻿using System;

namespace TouristCompany
{
    // Паттерн - Декоратор
    // Абстрактный класс туристических услуг
    abstract class TouristService : ITour
    {
        protected ITour tour;

        public TouristService(ITour tour)
        {
            this.tour = tour;
        }

        public virtual void DisplayInfo()
        {
            tour.DisplayInfo();
        }

        public virtual double GetPrice()
        {
            return tour.GetPrice();
        }

        public virtual void Accept(ITourVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    // Конкретный декоратор
    class TourWithInsurance : TouristService
    {
        public TourWithInsurance(ITour tour) : base(tour)
        {
        }

        public override void DisplayInfo()
        {
            base.DisplayInfo();
            Console.WriteLine(" + with insurance");
        }

        public override double GetPrice()
        {
            return base.GetPrice() * 1.2; // увеличиваем цену на 20%
        }
    }

    // Паттерн - Посетитель:
    // Интерфейс посетителя
    interface ITourVisitor
    {
        void Visit(ITour tour);
    }

    // Конкретный посетитель
    class USClient : ITourVisitor
    {
        public void Visit(ITour tour)
        {
            // Рассчитываем стоимость тура для американских клиентов
            double cost = tour.GetPrice() * 0.8;
            Console.WriteLine($"The cost for US client is: {cost}");
        }
    }

    // Конкретный посетитель
    class EUClienct : ITourVisitor
    {
        public void Visit(ITour tour)
        {
            // Рассчитываем стоимость тура для европейских клиентов
            double cost = tour.GetPrice() * 1.2;
            Console.WriteLine($"The cost for EU client is: {cost}");
        }
    }

    // Интерфейс для создания туров
    interface ITour
    {
        void DisplayInfo();
        double GetPrice();
        void Accept(ITourVisitor visitor);
    }

    // Конкретные туры
    class SkiTour : ITour
    {
        public void DisplayInfo()
        {
            Console.WriteLine("This is a Ski tour.");
        }

        public double GetPrice()
        {
            return 1000;
        }

        public void Accept(ITourVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    class BeachTour : ITour
    {
        public void DisplayInfo()
        {
            Console.WriteLine("This is a Beach tour.");
        }

        public double GetPrice()
        {
            return 800;
        }

        public void Accept(ITourVisitor visitor)
        {
            visitor.Visit(this);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {

           
            // Создаем объекты клиентов
            var usClient = new USClient();
            var euClient = new EUClienct();

            // Создаем объект тура
            ITour tour = new SkiTour(); // создаем тур
            var tour2 = new TourWithInsurance(tour); // добавляем услугу страховки
            tour2.DisplayInfo(); // выводим информацию о туре с услугой страховки

            // Рассчитываем стоимость тура для американских клиентов
            tour.Accept(usClient);

            // Рассчитываем стоимость тура для европейских клиентов
            tour.Accept(euClient);

            // Создаем объекты декораторов
            var tourWithInsurance = new TourWithInsurance(tour);

            // Рассчитываем стоимость тура для американских клиентов с учетом декоратора
            tourWithInsurance.Accept(usClient);

            // Рассчитываем стоимость тура для европейских клиентов с учетом декоратора
            tourWithInsurance.Accept(euClient);

            Console.ReadKey();
        }
    }
}

